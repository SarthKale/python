import json

def read_json(file_path):
    # reads the json file and returns a json object
    with open(file_path, "r") as file:
        json_data = json.load(file)
        
    return json_data

def parse_header(json_data: list) -> str:
    # parse the header from the given json_data
    header = ""
    for key in json_data[0]:
        header += key + ","
    return header


def write_json(file_path, header, rows: list):
    # writes rows in text format into a csv file 
    with open(file_path, "w") as file:
        file.write(header + "\n")

        count = 1
        for row in rows:
            file.write("\nid : " + str(count)  + ",\nfields: {\n\t")
            file.write(row +"\n")
            file.write("}")
            count += 1


def extract_rows(header, json_data) -> list:
    rows = []
    for row in json_data:
        res = []
        for value in row.items():
            res.append(str(value))
        rows.append("\n\t".join(res[1:]))
    return rows


def main():
    json_data = read_json("input.json")
    header = parse_header(json_data)[:-1]
    rows = extract_rows(header, json_data)
    write_json("output1.json", header, rows)


if __name__ == '__main__':
    main()
