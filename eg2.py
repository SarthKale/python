import json


def read_json():
    with open('input.json', 'r') as file:
        data = json.load(file)
    return data


def parse_json(data: list):
    res = []
    for block in data:
        res.append(parse_block(block))
    return res


def parse_block(block: dict):
    res = {}
    res['id'] = block.get('id', None)
    res['fields'] = {}
    for key, value in block.items():
        if key != 'id':
            res['fields'].update({key: value})
    return res


print(json.dumps(parse_json(read_json()), indent=2))
