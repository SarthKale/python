"""Module written in order to solve a problem statement."""


def calculate1(num_list, operation="add"):
    """calculate() function computes a result based on the operation
    provided from a list of integers.

    Parameters:
    num_list:list -> It's a list of integers.
    operation:str -> Operation, example: add, subtract, multiply, divide

    Returns:
    result: int -> Computed value.

    """
    pass

"""
print(calculate([4, 5, 6, 4], operation="add") == 19)
print(calculate([4, 0, 8, 4], operation="subtract") == -8)
print(calculate([4, 5, 6, 4], operation="multiply") == 480)
print(calculate([20, 5], operation="divide") == 4)
print(calculate([]) == -1)
print(calculate([], operation="add") == -1)
print(calculate([10, 20, 30, 40]) == 100)
"""


def calculate(args = [-1], operation:str = "add"):
    """
    This function performs an operation on the given list.
    arguments:
        1. args -> It accepts the list of numbers on which the given operation is to be performed.
        2. operation -> It accepts the operation type to be performed on the given list.
    It returs  an int as the result accordin to the operation performed on the list.
    """
    if args[0] == -1:
        return -1
    
    else:
        if operation == "add":
            sum=0
            for item in args:
               sum += item  
            return sum      
        if operation == "subtract":
            dif = args[0]
            for item in args:
               dif -= item
            dif += args[0]
        if operation == "multiply":
            prod = 1
            for item in args:
                prod *= item
            return prod
        if operation =="division":
            que = args[0]
            for item in args:
               que //= item
            que *= args[0]
            return que
my_list = [2, 3, 4, 5, 6]
my_string = "multiply"
print(calculate([2, 3, 4, 5, 6], my_string))
