"""
Given the names and grades for each student in a Physics class of  students, store them in a nested list and print the name(s) of any student(s) having the second lowest grade.

Note: If there are multiple students with the same grade, order their names alphabetically and print each name on a new line.

Input Format

The first line contains an integer, , the number of students.
The  subsequent lines describe each student over  lines; the first line contains a student's name, and the second line contains their grade.

Constraints

There will always be one or more students having the second lowest grade.
Output Format

Print the name(s) of any student(s) having the second lowest grade in Physics; if there are multiple students, order their names alphabetically and print each one on a new line.
"""

from math import inf
students = []
for _ in range(int(input())):
    name = input()
    score = float(input())
    students.append([name, score])

scores = []
for student in students:
    scores.append(student[1])

set_score = set(scores)
def sec_lowest_score(set_score):
    ls, sls = float(inf), float(inf)
    for score in set_score:
        if ls >= score:
            ls, sls = score, ls
        elif score < sls:
            sls = score
    return sls

sls = sec_lowest_score(set_score)

result = []
for student in students:
    if student[1] == sls:
        result.append(student[0])

print("\n".join(sorted(result)))
    
""" 
Is not giving any output

5
Harry
37.21
Berry
37.21
Tina
37.2
Akriti
41
Harsh
39
"""